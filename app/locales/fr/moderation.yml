title: Charte de modération pour les espaces gérés par Framasoft
h2-intro: Préambule
p-intro:
- Framasoft met à disposition un certain nombre d’espaces d’échanges. Ces espaces
  regroupant des personnes d’horizons et d’éducations différentes, nous rêvons d’un
  monde de licornes et d’arcs-en-ciel, où chacun et chacune pourrait échanger et
  apprendre des autres en toute quiétude. Néanmoins, nous avons conscience que ce
  monde parfait n’existe pas et que nous devons proposer un cadre pour les interactions
  au sein des espaces que nous proposons.
ul-intro:
- Nous voulons discuter et échanger avec des gens ayant des opinions différentes,
  de façon courtoise et respectueuse, afin d’enrichir notre vision du monde.
- Nous sommes conscient⋅e⋅s que des personnes peuvent être en situation de vulnérabilité.
- Nous proposons de vérifier auprès de ces personnes si elles ont besoin d’une aide
  et de la leur fournir si cela nous est possible.
p1-intro:
- 'Pour que ces échanges puissent se dérouler dans un tel cadre, cette charte définit
  les comportements que nous encourageons et ceux que nous refusons. L’implication
  de tout le monde est nécessaire : sans signalement, nous ne pourrons pas grand
  chose, et tout le monde doit avoir connaissance de ces règles pour les appliquer.
  Nous vous encourageons donc à diffuser cette charte autant que possible.'
- Certains des comportements que nous refusons sont interdits par la loi, que nous
  rappelons ici. D’autres comportements, non encadrés par la loi mais précisés dans
  ce document, peuvent également être source de souffrance et nous les modérerons
  aussi.
h2-champ: 0 - Champ d’application
p-champ:
- 'Cette charte s’applique à toute personne intervenant au sein de Framasoft, notamment
  sur nos sites et services. Cela inclut :'
ul-champ:
- les membres de l’association Framasoft ;
- les bénévoles ;
- les utilisateurs et utilisatrices, y compris en visite rapide.
h2-cadrelegal: 1 - Cadre légal
p-cadrelegal:
- Nous sommes placé·e⋅s sous l’autorité de la loi française, et les infractions
  que nous constaterons sur nos médias pourront donner lieu à un dépôt de plainte.
  Elles seront aussi modérées afin de ne plus apparaître publiquement.
- 'Cela concerne entre autres (sans être exhaustif) :'
ul-cadrelegal:
- <a href="https://www.service-public.fr/particuliers/vosdroits/F32077">l’injure</a>
- <a href="https://www.service-public.fr/particuliers/vosdroits/F32079">la diffamation</a>
- <a href="https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006417831&cidTexte=LEGITEXT000006070719">la
  discrimination</a>
- <a href="https://www.legifrance.gouv.fr/affichCode.do;jsessionid=856A2CA1AFD13F30FDD5E861BD6111CE.tplgfr38s_3?idSectionTA=LEGISCTA000006181752&cidTexte=LEGITEXT000006070719&dateTexte=20180226">les
  menaces</a>
- <a href="https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=856A2CA1AFD13F30FDD5E861BD6111CE.tplgfr38s_3?idArticle=LEGIARTI000029334247&cidTexte=LEGITEXT000006070719&dateTexte=20180226">le
  harcèlement moral</a>
- <a href="https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006417929&idSectionTA=LEGISCTA000006165309&cidTexte=LEGITEXT000006070719&dateTexte=20180226">l’atteinte
  à la vie privée</a>
- <a href="https://fr.wikipedia.org/wiki/Lois_contre_le_racisme_et_les_discours_de_haine#France">les
  appels à la haine</a>
- 'La protection des mineurs, en particulier face à la présence de contenu à caractère
  pornographique : <ul><li><a href="https://www.legifrance.gouv.fr/affichTexte.do;jsessionid=16626A12E54D4AEF48F5E872C1CE9363.tplgfr28s_3?cidTexte=JORFTEXT000000556901&dateTexte=20190710">Loi
  n° 98-468 du 17 juin 1998</a></li><li><a href="https://www.legifrance.gouv.fr/affichCode.do;jsessionid=B900212578B680D29FB5882C9E4D9DC6.tplgfr28s_3?idSectionTA=LEGISCTA000006165321&cidTexte=LEGITEXT000006070719&dateTexte=20190616">Article
  227-22-1 et Article 227-23 du code pénal</a></li></ul>'
h2-comportement: 2 - Autres comportements modérés
p-comportement:
- 'Nous n’accepterons pas les comportements suivants, qui seront aussi modérés :'
ul-comportement:
- Envoyer des messages agressifs à une personne ou à un groupe, quelles que soient
  les raisons. Si on vous insulte, signalez-le, n’insultez pas en retour.
- Révéler les informations privées d’une personne. Dévoiler ses pseudonymes et les
  caractères de son identité est un choix personnel et individuel qui ne peut être
  imposé.
- Insister auprès d’une personne alors que cette dernière vous a demandé d’arrêter.
  Respectez les refus, les « non », laissez les gens tranquilles s’iels ne veulent
  pas vous parler.
- Faire des blagues qui ne  nous font pas rire. Nous n’aurons pas d’humour notamment
  sur des questions de genre, d’orientation sexuelle, d’apparence physique, de culture,
  de validité physique ou mentale, etc. et plus généralement sur tout ce qui entretient
  des oppressions systémiques. Dans le doute, demandez-vous préalablement si les
  personnes visées vont en rire avec vous.
- Générer des messages automatiques de type flood, spam, etc. Les robots sont parfois
  tolérés, à condition d’être bien éduqués.
- Partager des émotions négatives intenses (colère, dépression), en particulier
  à l’égard d’une personne. Les médias sociaux ne sont pas un lieu approprié pour
  un règlement de compte ou une demande de prise en charge psychologique. Quittez
  votre clavier pour exprimer les émotions intenses, trouvez le lieu adéquat pour
  les partager, puis revenez (si nécessaire) exposer calmement vos besoins et vos
  limites.
- Participer à un harcèlement par la masse, inciter à l’opprobre générale et à l’accumulation
  des messages (aussi appelé <a href="https://www.urbandictionary.com/define.php?term=dogpile">dogpiling</a>).
  Pour vous ce n’est peut-être qu’un unique message, mais pour la personne qui le
  reçoit c’est un message de trop après les autres.
- Dénigrer une personne, un comportement, une croyance, une pratique. Vous avez
  le droit de ne pas aimer, mais nous ne voulons pas lire votre jugement.
h2-personnes: 3 - Les personnes ne sont pas leurs comportements
p-personnes:
- Nous différencions personnes et comportements. Nous modérerons les comportements
  enfreignant notre code de conduite comme indiqué au point 5, mais nous ne bannirons
  pas les personnes sous prétexte de leurs opinions sur d’autres médias.
- 'Cependant, nous bannirons toute personne ayant mobilisé de façon répétée nos
  équipes de modération : nous voulons échanger avec les gens capables de comprendre
  et respecter notre charte de modération.'
h2-outils: 4 - Outils à votre disposition
p-outils:
- 'Nous demandons à chacun⋅e d’être vigilant⋅e. Utilisons les outils à notre disposition
  pour que cet espace soit agréable à vivre :'
ul-outils:
- 'Si quelqu’un vous pose problème ou si vous vous sentez mal à l’aise dans une
  situation, ne vous servez pas de votre souffrance comme justification pour agresser.
  Vous avez d’autres moyens d’actions (détaillés dans la <a href="https://docs.framasoft.org/fr/moderation/">documentation</a>)
  : <ul> <li>fuir la conversation, </li> <li>bloquer le compte indésirable</li>
  <li>bloquer l’instance entière à laquelle ce compte est rattaché (uniquement disponible
  sur Mastodon)</li> <li>signaler le compte à la modération.</li> </ul>'
- Si quelqu’un vous bloque, entendez son souhait et ne cherchez pas à contacter
  cette personne par un autre biais, ce sera à elle de le faire si et quand elle
  en aura envie.
- Si vous constatez des comportements contraires au code de conduite menant des
  personnes à être en souffrance, signalez-le à la modération afin de permettre
  l’action des modérateurs et modératrices. Alerter permet d’aider les personnes
  en souffrance et de stopper les comportements indésirables.
- S’il n’est pas certain qu’une personne est en souffrance, mais que les comportements
  à son égard sont discutables, engagez la conversation autour de ces comportements
  avec chacune des personnes concernées, dans le respect, afin d’interroger et de
  faire évoluer votre perception commune des comportements en question.
- 'Dans le cadre du fediverse (Framapiaf, PeerTube, Mobilizon, …), vous pouvez également
  masquer un compte (vous ne verrez plus ses messages dans votre flux mais vous
  pourrez continuer à discuter avec la personne). Vous pouvez aussi bloquer un compte
  ou une instance entière depuis votre compte : à ce moment, plus aucune interaction
  ne sera possible entre vous, jusqu’à ce que vous débloquiez.'
p1-outils:
- Référez-vous à la <a href="https://docs.framasoft.org/fr/moderation/">documentation</a>
  sur les façons de signaler et se protéger suivant les médias utilisés.
h2-action: 5 - Actions de modération
p-action:
- Lorsque des comportements ne respectant pas cette charte nous sont signalés, nous
  agirons dans la mesure de nos moyens et des forces de nos bénévoles.
- 'Dans certains cas, nous essayerons de prendre le temps de discuter avec les personnes
  concernées. Mais il est aussi possible que nous exécutions certaines de ces actions
  sans plus de justification :'
ul-action:
- suppression du contenu (des messages, des vidéos, des images, etc.)
- modification (ou demande de modification) de tout ou partie du contenu (avertissement
  de contenu, suppression d’une partie)
- bannissement de compte ou d’instance, en cas de non respect des CGU (usage abusif
  des services) ou de non-respect de cette charte.
p-nota:
- 'NB : Dans le cadre du fediverse (Framapiaf, PeerTube, Mobilizon, …), nous pouvons
  masquer les contenus problématiques relayés sur notre instance, voir bloquer des
  instances problématiques, mais non agir directement sur les autres instances.'
- 'La modération est effectuée par des bénévoles (et humain⋅es !), qui n’ont pas
  toujours le temps et l’énergie de vous donner des justifications. Si vous pensez
  avoir été banni⋅e ou modéré⋅e injustement : nous sommes parfois injustes ; nous
  sommes surtout humain⋅e⋅s et donc faillibles. C’est comme ça.'
- Utiliser nos services implique d’accepter nos <a href="https://framasoft.org/fr/cgu">conditions
  d’utilisation</a>, dont cette charte de modération.
h2-desaccord: 6 - En cas de désaccord
p-desaccord:
- 'Si la façon dont l’un de nos services est géré ne vous plaît pas :'
ul-desaccord:
- Vous pouvez aller ailleurs ;
- Vous pouvez bloquer toute notre instance.
p1-desaccord:
- Framasoft ne cherche pas à centraliser les échanges et vous trouverez de nombreux
  autres hébergeurs proposant des services similaires, avec des conditions qui vous
  conviendront peut-être mieux. Que les conditions proposées par Framasoft ne vous
  conviennent pas ou que vous ayez simplement envie de vous inscrire ailleurs, nous
  essaierons de faciliter votre éventuelle migration (dans la limite de nos moyens
  et des solutions techniques utilisées).
h2-evolution: 7 - Évolution de la Charte
p-evolution:
- Toute « Charte de modération » reste imparfaite car il est très difficile de l’adapter
  à toutes les attentes. Nous restons à votre écoute afin de le faire vivre et de
  l’améliorer. Pour cela vous pouvez contacter les modérateurs et modératrices pour
  leur faire part de vos remarques.
footer:
  p:
  - Charte de modération approuvée par<br>l’association @:color.soft le 8 juillet
    2019.
